// Made by Borhan Amini and Juuso Minkkilä
// April 2017 JAMK

#include "msp430g2553.h"
#include <stdio.h>

long temp;	// Stores the "temperature number" from ADC10MEM
int tempC;	// Stores temperature in degrees celcius
int n1;		// Number for digit 2
int n2;		// Number for digit 3
int digit;	// Controls digit
int loops;	// Number of loops the timer has ran

void main(void)
{
	WDTCTL = WDTPW + WDTHOLD;					// Stop WDT
	ADC10CTL1 = INCH_10 + ADC10DIV_3;			// Temp Sensor ADC10CLK/4
	ADC10CTL0 = SREF_1 + ADC10SHT_3 + REFON + ADC10ON + ADC10IE;
	__enable_interrupt();						// Enable interrupts.
	
	// initialization, all ports 0
	P1DIR = 0xFF;
	//P1SEL = 0x00;
	
	P2DIR = 0xFF;
	//P2SEL = 0x00;
	
	// attempting to get 8 to digits 2 and 3
	//P1OUT = 0x08;	// P1.3 output
	
	//P2OUT = 0x20;
	
	tempC = 0;
	n1 = 0;
	n2 = 0;
	digit = 0;
	loops = 0;
	
	// timer stuff
	CCTL0 = CCIE;	// CCR0 interrupt enabled
	TACTL = TASSEL_2 + MC_1 + ID_3;	// SMCLK/8, upmode  
	CCR0 = 125;		// 1 kHz 
	
	// PWM stuff
	
	TA1CTL	= TASSEL_2 + MC_1 + ID_0;	// SMCLK, UP-mode, no divider
	TA1CCTL1	= OUTMOD_7;	// Output Mode 7, SET/RESET
	TA1CCR0 = 0xFFF;	// Count to 0xFFF and restart
	P2SEL |= 0x02;	// P2.1
	
	
	//TA1CCR1 = 0x800;
	
	while(1){
		ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start
		__bis_SR_register(CPUOFF + GIE);        // LPM0 with interrupts enabled
	}
}

// ADC10 interrupt service routine
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR (void) {
	
	if (loops > 999)
	{
		loops = 0;
		temp = ADC10MEM;
	
		tempC = temp * 0.41 - 278;
		
		//printf("%i \n", tempC);
		//printf("%i \n", temp);
		
		n1 = abs(tempC / 10);
		n2 = abs(tempC % 10);
		
		// PWM
	}
	__bic_SR_register_on_exit(CPUOFF);        // Clear CPUOFF bit from 0(SR)
}

// Timer A0 interrupt service routine 
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A0 (void) {
	//printf("T\n");
	
	if (digit == 0)
	{
		if (tempC < 0)
		{
			P1OUT = 0x1A;
			//printf("-\n");
		}
		else 
		{
			P1OUT = 0x0A;
			// 0x0A is a blank digit
		}
		
		P2OUT = 0x08;	// P2.3, digit 1
		digit++;
		//printf("T0\n");
	}
	else if (digit == 1)
	{
		if (n1 == 0)
		{
			P1OUT = 0x0A;
			// 0x0A is a blank digit
		}
		else 
		{
			P1OUT = n1;
		}
		
		P2OUT = 0x10;	// P2.4, digit 2
		digit++;
		//printf("%i \n", n1);
	}
	else
	{
		P1OUT = n2;
		P2OUT = 0x20;	// P2.5, digit 3
		digit = 0;
		//printf("%i \n", n2);
	}
	
	if (tempC > 20)
		{
			
			int myTA = tempC / 2 * 0xFF;
			if (myTA > 0xFFF) TA1CCR1 = 0xFFF;
			else TA1CCR1 = myTA;
			
			
			//TA1CCR1 = tempC / 2 * 0xFF;
		}
	
	loops++;
	__bic_SR_register_on_exit(CPUOFF);	// Clear CPUOFF bit from 0(SR)
}
